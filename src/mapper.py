#!/usr/bin/env python
#Mapping step
import sys
import re


class Mapper(object):
    def __init__(self, input=sys.stdin, output=sys.stdout):
        #input and output streams are treated as parameters
        self.str = {}
        #input words will be here
	self.MaxLength = 0
        #containts maximum legnth obviously
        self.input = input
        self.output = output
        

    def write(self):
        #output
        for (key, value) in self.str.items():
            self.output.write(key + '\t' + str(value) + '\n')

    def map(self):
        #mapping step itself
        for token in self.input:
            for word in re.finditer(r'\b([a-zA-Z]+)\b', token):
                if len(word.group()) < 4:
                    print >> sys.stderr, "reporter:counter: CUSTOM, ShortWords,1"
                    #counting short words
                    #counter is passed through stderr
                elif self.MaxLength < len(word.group()):
                    self.MaxLength = len(word.group())
                    self.str = {word.group(): self.MaxLength}
                elif word.group() not in self.str and self.MaxLength == len(word.group()):
                    self.str[word.group()] = self.MaxLength
        self.write()


if __name__ == '__main__':
    mapper = Mapper()
    mapper.map()

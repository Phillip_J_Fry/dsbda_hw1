#!/usr/bin/env python
#Reducing step
import sys


class Reducer(object):
    def __init__(self, input=sys.stdin, output=sys.stdout):
        #input and output strems are treated as parameters
        self.str = {}
	self.MaxLength = 0
        self.input = input
        self.output = output
        

    def write(self):
        #output
        for (key, value) in self.str.items():
            self.output.write(key + '\t' + str(value) + '\n')

    def reduce(self):
        #reducing step itself
        for token in self.input:
            (key, value) = token.strip().strip('\n').split('\t')
            if self.MaxLength < int(value):
                self.MaxLength = int(value)
                self.str = {key: self.MaxLength}
            elif key not in self.str and self.MaxLength == int(value):
                self.str[key] = self.MaxLength
        self.write()


if __name__ == '__main__':
    reducer = Reducer()
    reducer.reduce()

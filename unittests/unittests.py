#!/usr/bin/env python
#some unit tests
import unittest
import io
import mapper as mpr
import reducer as rdr


class MapTesting(unittest.TestCase):
    def test_one_word(self):
		#test with one word
        inputstr = 'Something'
        output = io.StringIO()
        mapper = mpr.Mapper(input=io.StringIO(inputstr), output=output)
        mapper.map()
        self.assertEqual(
            output.getvalue().strip(),
            inputstr + ', ' + str(len(inputstr))
        )

    def test_equal_length_words(self):
		#test with 4 words of equal lengths
        inputstr = ['Fooo', 'Baar', 'Woow', 'Keek']
        output = io.StringIO()
        mapper = mpr.Mapper(input=io.StringIO(', '.join(inputstr)), output=output)
        mapper.map()
        self.assertEqual(
            sorted(output.getvalue().strip().split('\n')),
            sorted(map(lambda x: x + ', ' + str(len(x)), inputstr))
        )

    def test_different_words_string(self):
		#test with different kind of words including
		#words with len < 4
        inputstr = ['Foo', 'Bar', 'Wow', 'Doge', 'LongWord']
        MaxLength = max(map(len, inputstr))
        LongestWord = list(filter(lambda x: len(x) == MaxLength, inputstr))[0]
        output = io.StringIO()
        mapper = mpr.Mapper(input=io.StringIO(', '.join(inputstr)), output=output)
        mapper.map()
        self.assertEqual(
            output.getvalue().strip(),
            LongestWord + ', ' + str(MaxLength)
        )



class ReduceTesting(unittest.TestCase):
    def test_one_word_string(self):
		#same 1 word test for reducer
        inputstr = 'Something, 9'
        output = io.StringIO()
        reducer = rdr.Reducer(input=io.StringIO(inputstr), output=output)
        reducer.reduce()
        self.assertEqual(
            output.getvalue().strip(),
            inputstr
        )

    def test_equal_length_words(self):
		#same as for mapper
        inputstr = ['Fooo, 4', 'Baar, 4', 'Woow, 4', 'Keek, 4']
        output = io.StringIO()
        reducer = rdr.Reducer(input=io.StringIO('\n'.join(inputstr)), output=output)
        reducer.reduce()
        self.assertEqual(
            sorted(output.getvalue().strip().split('\n')),
            sorted(inputstr)
        )

    def test_different_words_string(self):
		#same as for mapper
        inputstr = ['Foo, 3', 'Bar, 3', 'Wow, 3', 'Doge, 4', 'LongWord, 8']
        MaxLength = max(map(len, inputstr))
        LongestWord = list(filter(lambda x: len(x) == MaxLength, inputstr))[0]
        output = io.StringIO()
        reducer = rdr.Reducer(input=io.StringIO('\n'.join(inputstr)), output=output)
        reducer.reduce()
        self.assertEqual(
            output.getvalue().strip(),
            LongestWord
        )


if __name__ == '__main__':
    unittest.main()

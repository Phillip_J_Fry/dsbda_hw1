#!/usr/bin/env python
#Mapping step
import sys
import re


class Mapper(object):
    def __init__(self, input=sys.stdin, output=sys.stdout):
        #input and output stremas are treated as parameters
        self.str = {}
        self.MaxLength = 0
        self.input = input
        self.output = output
        

    def write(self):
        #output
        for (key, value) in self.str.items():
            self.output.write(key + ', ' + str(value) + '\n')

    def map(self):
        #mapper itself
        for token in self.input:
            for word in re.finditer(r'\b([a-zA-Z]+)\b', token):
                if len(word.group()) < 4:
                    print("counter+1", sys.stderr)
                    #This part of code is changed due to test on host system, not on hadoop
					#different python verisons used; no counters exist in stderr on host system
					#outside of hadoop
                    #print >> sys.stderr, "reporter:counter: CUSTOM, ShortWords,1"
                    #counting short words that are probably articles
                elif self.MaxLength < len(word.group()):
                    self.MaxLength = len(word.group())
                    self.str = {word.group(): self.MaxLength}
                elif word.group() not in self.str and self.MaxLength == len(word.group()):
                    self.str[word.group()] = self.MaxLength
            self.write()


if __name__ == '__main__':
    mapper = Mapper()
    mapper.map()

#!/usr/bin/env python
#Reducing step
import sys


class Reducer(object):
    def __init__(self, input=sys.stdin, output=sys.stdout):
        #input and output streams are treated as parameters
        self.str = {}
        self.MaxLength = 0
        self.input = input
        self.output = output
        

    def write(self):
        #output
        for (key, value) in self.str.items():
            self.output.write(key + ', ' + str(value) + '\n')

    def reduce(self):
        #reducer itself
        for token in self.input:
            (key, value) = token.strip().strip('\n').split(', ')
            if self.MaxLength < int(value):
                self.MaxLength = int(value)
                self.str = {key: self.MaxLength}
            elif key not in self.str and self.MaxLength == int(value):
                self.str[key] = self.MaxLength
        self.write()


if __name__ == '__main__':
    reducer = Reducer()
    reducer.reduce()

#!/usr/bin/env bash
hadoop fs -rm -r 4hdfs/MapReduce 
hadoop fs -mkdir -p 4hdfs/MapReduce 
hadoop fs -mkdir 4hdfs/MapReduce/input 
hadoop fs -mkdir 4hdfs/MapReduce/src 
hadoop fs -put ./MapReduce/samples/*.txt /user/cloudera/4hdfs/MapReduce/input 
hadoop fs -put ./MapReduce/src/*.py /user/cloudera/4hdfs/MapReduce/src
